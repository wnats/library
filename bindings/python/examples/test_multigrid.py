# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import json
import numpy as np
import matplotlib.pyplot as plt

from geostack.runner import runScript
from geostack.raster import Raster, RasterPtrList
from geostack.solvers import Multigrid

# Create raster layer
base = Raster(name = "base")

# Create input list
base.init(nx = 1000, ny = 1000, hx = 1.0, hy = 1.0, ox = -500.0, oy = -500.0)

runScript('''
    base = exp(-(pow(x-200, 2)+pow(y, 2))/100.0)-
           exp(-(pow(x+200, 2)+pow(y, 2))/100.0);
    ''', [base])

# Create solver
solver = Multigrid()
inputLayers = RasterPtrList()
inputLayers.append(base)
config = {
    "initialisationScript": "b = base;"
}
solver.init(json.dumps(config), inputLayers)

# Solve
solver.step()

# Get solution
solution = solver.getSolution()
solution.setProperty("name", "solution")
dims = solution.getDimensions()

# Calculate gradient
gx = Raster(name = "gx")
gy = Raster(name = "gy")
gx.initWithDimensions(dims)
gy.initWithDimensions(dims)
runScript('''
    REALVEC2 g = grad(solution);
    g = normalize(g);
    gx = g.x; 
    gy = -g.y;''', [solution, gx, gy])

# Plot
N = 50
x, y = np.meshgrid(np.arange(dims.ox, dims.ex, 1), np.arange(dims.oy, dims.ey, 1))
fig, ax = plt.subplots()
ax.quiver(x[::N, ::N], y[::N, ::N], gx.data[::N, ::N], gy.data[::N, ::N], pivot='mid', scale = 20)
plt.imshow(solution, extent=[dims.ox, dims.ex, dims.oy, dims.ey], cmap=plt.cm.coolwarm)
plt.show()

print("Done")