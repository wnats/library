# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import json
import numpy as np

import matplotlib.pyplot as plt

from geostack.vector import Vector
from geostack.solvers import Particle
from geostack.io import vectorToGeoJson

# Create initial conditions
particles = Vector()
particles.addPoint( [1, 0, 0] )

# Create solver
config = {
    "dt": 0.01,
    "initialisationScript" : '''
        velocity = (REALVEC3)(0.0, 0.5, 0.0);
    ''',
    "advectionScript" : '''
        time += dt;
    ''',
    "updateScript" : '''
        acceleration = -position*pow(length(position), -3.0);
    '''
}
solver = Particle()
solver.init(json.dumps(config), particles)

# Run solver
c = [particles.getCoordinate(0)]
for iter in range(0, 1000):
    solver.step()    
    c.append(particles.getCoordinate(0))
      
# Unpack lists
x, y, z, t = zip(*c)

# Plot
fig = plt.figure()
plt.scatter(x, y, c=t, cmap="viridis")
plt.plot([0], [0], 'kx')
plt.axis([-0.5, 1.5, -1, 1])
plt.show()

print("Done")