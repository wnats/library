# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import json
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

from geostack.solvers import ODE
from geostack.core import Variables

# Create solver instance
solver = ODE()

# Test 1:
#   solve eq.: dv/dt = -3.0*t^2*e^v
#   initial value: v(0) = 0
dt = 0.01
config = json.dumps({
    "dt": dt,
    "functionScripts": {
        "v": "return -3.0*t*t*exp(v);"
    },
    "outputSeries": { 
        "v": "time"
    }
})

# Create variables
time = 0.0
variables1 = Variables()
variables1.set("v", 0.0)

# Run solver
solver.init(config, variables1)
for _ in range(100):
    solver.step()
    time += dt
    
# Output value
numerical = variables1.get("v")
analytic = -math.log(math.pow(time, 3.0) + 1.0)
print(f"1. Numerical: {numerical}, analytic: {analytic}, error: {abs(numerical-analytic)}")

ta = []
va = []
for t in np.linspace(0, time, 10):
    ta.append(t)
    va.append(solver.get("v", "time", t))

# Plot
plt.figure()
plt.plot(ta, va)
plt.xlabel("time")
plt.ylabel("v")
plt.show()

# Test 2:
#   solve eq.: du/dt = u/t + 3*sqrt(t/u)
#   initial value: u(1) = 1, t = 1
dt = 0.01
config = json.dumps({
    "dt": dt,
    "functionScripts": {
        "u": "return (u/t)+3.0*sqrt(t/u);"
    }
})

# Create variables
time = 1.0
variables2 = Variables()
variables2.set("u", 1.0)

# Run solver
t = []
u = []
t.append(time)
u.append(variables2.get("u"))
solver.init(config, variables2)
solver.setTime(time)
for _ in range(100):
    solver.step()
    time += dt
    t.append(time)
    u.append(variables2.get("u"))
    
# Output value
numerical = variables2.get("u")
analytic = time*math.pow(4.5*math.log(time)+1.0, 2.0/3.0)
print(f"2. Numerical: {numerical}, analytic: {analytic}, error: {abs(numerical-analytic)}")

# Plot
plt.figure()
plt.plot(t, u)
plt.xlabel("time")
plt.ylabel("u")
plt.show()


# Test 3, Lotka–Volterra equations:
#   solve eqns.:
#       dx/dt = αx - βxy
#       dy/dt = δxy - γy
#       x(0) = 10.0
#       y(0) = 10.0

#   where:
#       α = 1.1
#       β = 0.4
#       δ = 0.1
#       γ = 0.4

dt = 0.1
config = json.dumps({
    "dt": dt,
    "functionScripts": [
        {
            "x": """REAL α = 1.1;
                    REAL β = 0.4;
                    return α*x-β*x*y;"""
        },
        {
            "y": """REAL δ = 0.1;
                    REAL γ = 0.4;
                    return δ*x*y-γ*y;"""
        }
        ]
    })

variables3 = Variables()
variables3.set("x", 10.0)
variables3.set("y", 10.0)

# Run solver
t = []
x = []
y = []
t.append(time)
x.append(variables3.get("x"))
y.append(variables3.get("y"))
solver.init(config, variables3)
for _ in range(1000):
    solver.step()
    time += dt
    t.append(time)
    x.append(variables3.get("x"))
    y.append(variables3.get("y"))

# Plot
plt.figure()
plt.title("Lotka–Volterra equations")
plt.plot(t, x)
plt.plot(t, y)
plt.xlabel("time")
plt.show()

# Test 4, Lorenz system:
#   solve eqns.:
#       dx/dt = σ(x - y)
#       dy/dt = x(ρ - z) - y
#       dz/dt = xy - βz
#       x(0) = 10.0
#       y(0) = 10.0
#       z(0) = 10.0

#   where:
#       σ = 10.0
#       ρ = 28.0
#       β = 8.0 / 3.0

dt = 0.01
config = json.dumps({
    "dt": dt,
    "functionScripts": [
        {
            "x": """REAL σ = 10.0;
                    return σ * (y - x);"""
        },
        {
            "y": """REAL ρ = 28.0;
                    return x * (ρ - z) - y;"""
        },
        {
            "z": """REAL β = 8.0 / 3.0;
                    return x * y - β * z;"""
        }]
})

variables4 = Variables()
variables4.set("x", 10.0)
variables4.set("y", 10.0)
variables4.set("z", 10.0)

# Run solver
t = []
x = []
y = []
z = []
t.append(time)
x.append(variables4.get("x"))
y.append(variables4.get("y"))
z.append(variables4.get("z"))
solver.init(config, variables4)
for _ in range(1000):
    solver.step()
    time += dt
    t.append(time)
    x.append(variables4.get("x"))
    y.append(variables4.get("y"))
    z.append(variables4.get("z"))

# Plot
plt.figure()
#ax.title("Lorenz system")
ax = plt.axes(projection = "3d")
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")
#ax.plot(x, y, z)
ax.scatter3D(x, y, z, s = 1, c = t, cmap = "viridis")
plt.show()

# Test 5, parabolic motion:
#   solve eqns:
#       d^2x/dt^2 = 0
#       d^2y/dt^2 = -1
#       u(0) = dx(0)/dt = 2
#       v(0) = dy(0)/dt = 2
#       x(0) = 0
#       y(0) = 0

dt = 0.1
config = json.dumps({
    "dt": dt,
    "adaptiveMaximumSteps": 10,
    "adaptiveTolerance": 1e-3,
    "functionScripts": [
        {
            "x": "return u;",
        },
        {
            "y": "return v;",
        },
        {
            "u": "return 0.0;",
        },
        {
            "v": "return -1;"
        }
    ],
    "outputSeries": { 
        "y": "x"
    }
})

variables5 = Variables()
variables5.set("x", 0.0, 0)
variables5.set("y", 0.0, 0)
variables5.set("u", 2.0, 0)
variables5.set("v", 2.0, 0)

variables5.set("x", 0.0, 1)
variables5.set("y", 0.0, 1)
variables5.set("u", 1.0, 1)
variables5.set("v", 4.0, 1)

# Run solver
t = []
t.append(time)
solver.init(config, variables5)
for _ in range(40):
    solver.step()
    time += dt
    t.append(time)

# Plot
plt.figure()
plt.title("Parabolic motion")

for index in range(0, 2):
    xa = []
    ya = []
    for x in np.linspace(0, 9, 20):
        xa.append(x)
        ya.append(solver.get("y", "x", x, index))
    plt.plot(xa, ya)

plt.xlabel("x")
plt.ylabel("y")
plt.show()
