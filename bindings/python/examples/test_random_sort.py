# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import numpy as np

from geostack.raster import Raster
from geostack.runner import runScript
from geostack.definitions import RasterSortType

def printRaster(r):
    v = []
    for k in range(0, 10):
        v.append(r.getCellValue(50, 50, k))
    print(v)

r = Raster(name="r")
r.init(nx = 100, ny = 100, nz = 10, hx = 1, hy = 1, hz = 1)

# Random, no sort
print("Random:")
runScript("r = random;", [r])
printRaster(r)

# Random, post-sort
print("Random with sort:")
runScript("r = random;", [r], RasterSortType.PostScript)
printRaster(r)

