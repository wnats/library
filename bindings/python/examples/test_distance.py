# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from geostack.vector import Vector
from geostack.raster import Raster

# Create raster layer
A = Raster(name = "A")

# Initialize Raster
A.init(
    nx = 300, ny = 300, nz = 2,
    hx = 0.01, hy = 0.01, hz = 1,
    ox = -1, oy = -1, oz = 0)

# Create vectors
v1 = Vector()
idx1 = v1.addPoint( [0.5, 0.5] )
v1.setProperty(idx1, "level", 0)
idx2 = v1.addPolygon( [ [ [1.0, 1.0], [1.0, 1.5], [1.5, 1.5], [1.5, 1.0] ], [ [1.1, 1.1], [1.1, 1.4], [1.4, 1.4], [1.4, 1.1] ] ] )
v1.setProperty(idx2, "level", 0)

v2 = Vector()
idx1 = v2.addPoint( [-0.5, -0.5] )
v2.setProperty(idx1, "level", 1)
v2.setProperty(idx1, "radius", 0.1)
idx2 = v2.addLineString( [ [-0.5, 1.5], [1.0, -0.5] ] )
v2.setProperty(idx2, "level", 1)
v2.setProperty(idx2, "radius", 0.25)

# Distance map
A.mapVector(v1, levelPropertyName="level")
A.mapVector(v2, levelPropertyName="level", widthPropertyName="radius")

# Write Raster
A.write('./_out_distance.tiff')

print("Done")
