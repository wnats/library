"""
Template for each `dtype` helper function for series

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# name, x_name, y_name

rtypes = [('d', 'double'),
         ('f', 'float'),]
}}

{{for x_name, x_type in rtypes}}

cdef class shapefile_{{x_name}}:

    @staticmethod
    cdef _Vector_{{x_name}} _shapefileToVector(string filename, string jsonConfig) except *:
        cdef Vector[{{x_type}}] _out
        _out = ShapeFile[{{x_type}}].shapefileToVector(filename, jsonConfig)

        out = _Vector_{{x_name}}()
        out.c_copy(_out)
        return out

    @staticmethod
    cdef _Vector_{{x_name}} _shapefileToVectorWithBounds(string filename,
                            BoundingBox[{{x_type}}] &bounds,
                            ProjectionParameters[double] &proj,
                            string jsonConfig) except *:
        cdef Vector[{{x_type}}] _out
        _out = ShapeFile[{{x_type}}].shapefileToVectorWithBounds(filename, bounds, proj, jsonConfig)

        out = _Vector_{{x_name}}()
        out.c_copy(_out)
        return out

    @staticmethod
    def shapefileToVector(filename, _BoundingBox_{{x_name}} boundingBox = None,
            _ProjectionParameters_d boundRegionProj = None, jsonConfig = ""):
        cdef BoundingBox[{{x_type}}] bounds

        if isinstance(jsonConfig, str):
            _jsonConfig = <bytes>jsonConfig.encode("utf-8")
        elif isinstance(jsonConfig, bytes):
            _jsonConfig = jsonConfig
        else:
            raise TypeError("jsonConfig is not of correct type")

        if boundingBox is None:
            if isinstance(filename, str):
                out = shapefile_{{x_name}}._shapefileToVector(<bytes>filename.encode('utf-8'), _jsonConfig)
            elif isinstance(filename, bytes):
                out = shapefile_{{x_name}}._shapefileToVector(filename, _jsonConfig)
        elif isinstance(boundingBox, (_BoundingBox_d, _BoundingBox_f)):
            bounds = deref(boundingBox.thisptr)

            if boundRegionProj is None:
                proj = _ProjectionParameters_d()
            else:
                proj = boundRegionProj

            if isinstance(filename, str):
                out = shapefile_{{x_name}}._shapefileToVectorWithBounds(<bytes>filename.encode('utf-8'),
                                                                        bounds, deref(proj.thisptr),
                                                                        _jsonConfig)
            elif isinstance(filename, bytes):
                out = shapefile_{{x_name}}._shapefileToVectorWithBounds(filename, bounds,
                    deref(proj.thisptr), _jsonConfig)

        return out

    @staticmethod
    cdef bool _vectorToShapefile(_Vector_{{x_name}} v, string filename,
            GeometryType geom_type) except *:
        cdef bool out
        out = ShapeFile[{{x_type}}].vectorToShapefile(deref(v.thisptr),
                filename, geom_type)
        return out

    @staticmethod
    def vectorToShapefile(_Vector_{{x_name}} v, string filename, int geom_type):
        out = shapefile_{{x_name}}._vectorToShapefile(v, filename,
            map_geom_type(geom_type))
        return out

{{endfor}}
