"""
Template for each `dtype` helper function

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# f_name, f_type

rtypes = [('d', 'double'),
          ('f', 'float'),]

}}

{{for f_name, f_type in rtypes}}

cdef class cyLevelSet_{{f_name}}:
    cdef unique_ptr[LevelSet[{{f_type}}]] thisptr

    def __cinit__(self):
        self.thisptr.reset(new LevelSet[{{f_type}}]())

    cpdef bool init(self, string jsonStartConditions,
        _Vector_{{f_name}} sources, _Variables_{{f_name}} variables,
        _RasterPtrList_{{f_name}} inp_layers,
        _RasterPtrList_{{f_name}} out_layers) except *:
        cdef bool out = False
        if self.thisptr != nullptr:
            with nogil:
                out = deref(self.thisptr).init(jsonStartConditions,
                    deref(sources.thisptr), variables.thisptr,
                    deref(inp_layers.thisptr), deref(out_layers.thisptr))
        return out

    cpdef uint64_t getEpochMilliseconds(self) except *:
        cdef uint64_t out
        out = deref(self.thisptr).getEpochMilliseconds()
        return out

    cpdef bool step(self) except *:
        cdef bool out = False
        with nogil:
            out = deref(self.thisptr).step()
        return out

    cpdef void resizeDomain(self, uint32_t nx, uint32_t ny, uint32_t tox, uint32_t toy) except *:
        with nogil:
            deref(self.thisptr).resizeDomain(nx, ny, tox, toy)

    cpdef void addSource(self, _Vector_{{f_name}} v) except *:
        deref(self.thisptr).addSource(deref(v.thisptr))

    cpdef _cyRaster_{{f_name}} getDistance(self) except *:
        cdef Raster[{{f_type}}, {{f_type}}] *r = &deref(self.thisptr).getDistance()
        out = _cyRaster_{{f_name}}()
        out.c_rastercopy(deref(r))
        return out

    cpdef _cyRaster_{{f_name}} getArrival(self) except *:
        cdef Raster[{{f_type}}, {{f_type}}] *r = &deref(self.thisptr).getArrival()
        out = _cyRaster_{{f_name}}()
        out.c_rastercopy(deref(r))
        return out

    cpdef _cyRaster_{{f_name}}_i getClassification(self) except *:
        cdef Raster[uint32_t, {{f_type}}] *r = &deref(self.thisptr).getClassification()
        out = _cyRaster_{{f_name}}_i()
        out.c_rastercopy(deref(self.thisptr).getClassification())
        return out

    cpdef _cyRaster_{{f_name}} getAdvect_x(self) except *:
        cdef Raster[{{f_type}}, {{f_type}}] *r = &deref(self.thisptr).getAdvect_x()
        out = _cyRaster_{{f_name}}()
        out.c_rastercopy(deref(r))
        return out

    cpdef _cyRaster_{{f_name}} getAdvect_y(self) except *:
        cdef Raster[{{f_type}}, {{f_type}}] *r = &deref(self.thisptr).getAdvect_y()
        out = _cyRaster_{{f_name}}()
        out.c_rastercopy(deref(r))
        return out

    cpdef _cyRaster_{{f_name}} getLevelSetLayer(self, uint32_t LevelSetLayer) except *:
        cdef Raster[{{f_type}}, {{f_type}}] *r = &deref(self.thisptr).getLevelSetLayer(LevelSetLayer)
        out = _cyRaster_{{f_name}}()
        out.c_rastercopy(deref(r))
        return out

    cpdef _cyRasterBase_{{f_name}} getOutput(self, string name) except *:
        cdef RasterBase[{{f_type}}] *r = &deref(self.thisptr).getOutput(name)
        cdef const char *capsule_name = "RasterBase"
        cdef object capsule = PyCapsule_New(
            <void*>r, capsule_name, NULL)
        cdef _cyRasterBase_{{f_name}} out = _cyRasterBase_{{f_name}}.from_ptr(
            capsule)
        return out

    def getParameters(self):
        cdef levelSetParams_{{f_name}} _out
        _out = deref(self.thisptr).getParameters()
        out = {
            "dt": _out.dt,
            "time": _out.time,
            "maxSpeed": _out.maxSpeed,
            "bandWidth": _out.bandWidth,
            "area": _out.area,
            "JulianDate": _out.JulianDate,
        }
        return out

    def setParameters(self, string param, {{f_type}} value):
        if param == b"dt":
            deref(self.thisptr).getParameters().dt = value
        elif param == b"time":
            deref(self.thisptr).getParameters().time = value
        elif param == b"maxSpeed":
            deref(self.thisptr).getParameters().maxSpeed = value
        elif param == b"bandWidth":
            deref(self.thisptr).getParameters().bandWidth = value
        elif param == b"area":
            deref(self.thisptr).getParameters().area = value
        elif param == b"JulianDate":
            deref(self.thisptr).getParameters().JulianDate = value
        else:
            raise KeyError("parameter is unknown")

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            if type(self) is cyLevelSet_{{f_name}}:
                self.thisptr.reset(nullptr)

    def __del__(self):
        try:
            self.__dealloc__()
        except Exception:
            pass

{{endfor}}