# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from __future__ import annotations
import json
from typing import Union, Dict, Any, AnyStr
import numpy as np
from ._cy_shallow_water import cyShallowWater_d, cyShallowWater_f
from ..raster import raster
from ..core import REAL, str2bytes

__all__ = ["ShallowWater"]


class ShallowWater:
    def __init__(self, dtype: np.dtype = REAL):  # type: ignore
        self._handle: Any = None
        self.dtype = dtype  # type: ignore
        if dtype is None or dtype == np.float32:
            self._handle = cyShallowWater_f()
            self.dtype = np.float32  # type: ignore
        elif dtype == np.float64:
            self._handle = cyShallowWater_d()
            self.dtype = np.float64  # type: ignore

    def init(self,
             jsonConfig: Union[Dict, AnyStr],
             h_in: Union['raster.Raster', 'raster.RasterFile'],
             b_in: Union['raster.Raster', 'raster.RasterFile']) -> bool:
        """initialise the solver instance.

        Parameters
        ----------
        jsonConfig : Union[Dict, str]
            a configuration for the solver
        h_in: Union[Raster, RasterFile]
            height raster
        b_in: Union[Raster, RasterFile]
            base raster

        Returns
        -------
        bool
            True if solver is initialised, False otherwise

        Raises
        ------
        TypeError
            jsonConfig should be a str or dict
        TypeError
            input layers should be an instance of RasterPtrList
        RuntimeError
            unable to initialise the solver
        RuntimeError
            ShallowWater solver is not instantiated
        """
        # Check types
        if not isinstance(jsonConfig, (str, bytes, dict)):
            raise TypeError("jsonConfig should be str or dict")

        assert isinstance(
            h_in, (raster.Raster, raster.RasterFile)), "height input should be a raster"
        assert isinstance(b_in, (raster.Raster, raster.RasterFile)), "base input should be a raster"

        # Convert json
        if isinstance(jsonConfig, (str, bytes)):
            _json_config = str2bytes(jsonConfig)
        elif isinstance(jsonConfig, dict):
            _json_config = str2bytes(json.dumps(jsonConfig))

        if isinstance(h_in, raster.Raster):
            height_obj = h_in._handle
        elif isinstance(h_in, raster.RasterFile):
            height_obj = h_in._handle.cy_raster_obj

        if isinstance(b_in, raster.Raster):
            base_obj = b_in._handle
        elif isinstance(b_in, raster.RasterFile):
            base_obj = b_in._handle.cy_raster_obj

        # Initialise
        if self._handle is not None:
            try:
                rc = self._handle.init(_json_config,
                                       height_obj, base_obj)
            except Exception as e:
                raise RuntimeError(f"Unable to initialise solver {str(e)}")
        else:
            raise RuntimeError("ShallowWater solver is not initialized")
        return rc

    def step(self) -> bool:
        """forward step the solver.

        Returns
        -------
        bool
            True is successful in forward stepping solver, False otherwise

        Raises
        ------
        RuntimeError
            solver is not instantiated
        """
        if self._handle is not None:
            return self._handle.step()
        else:
            raise RuntimeError("ShallowWater solver is not initialized")

    def __del__(self):
        if self._handle is not None:
            del self._handle
            self._handle = None

    def __exit__(self):
        self.__del__()

    def __setstate__(self, ds: Dict) -> None:
        self.__init__(dtype=ds.get('dtype'))

    def __getstate__(self) -> Dict:
        output = {"dtype": self.dtype}
        return output

    def __repr__(self):
        return "<class 'geostack.solvers.%s'>" % self.__class__.__name__
