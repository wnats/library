.. geostack documentation master file, created by
   sphinx-quickstart on Sun Mar  8 16:59:06 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to geostack's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

   geostack/core
   geostack/dataset
   geostack/io
   geostack/raster
   geostack/readers
   geostack/runner
   geostack/series
   geostack/solvers
   geostack/utils
   geostack/vector
   geostack/writers

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
