/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#ifndef GEOSTACK_OPERATION_H
#define GEOSTACK_OPERATION_H

#include "yaml.hpp"
#include "json11.hpp"
#include "gs_raster.h"
#include "gs_vector.h"

namespace Geostack
{

    using jsonPair = const std::pair<std::string, json11::Json>;

    json11::Json readYaml(std::string confileFile);
    json11::Json yamlToJson(Yaml::Node &yItemIn);

    /**
    * %Operation set.
    */
    template <typename T>
    class Operation {

    public:

        static void runFromConfigFile(std::string configFileName);

        // Initialisation
        static void run(
            std::string jsonConfig,
            std::vector<RasterBasePtr<T> > &rasters = std::vector<RasterBasePtr<T> >(),
            std::vector<VectorPtr<T> > &vectors = std::vector<VectorPtr<T> >());
    };
}
#endif
