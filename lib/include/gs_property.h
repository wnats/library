/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef GEOSTACK_PROPERTY_H
#define GEOSTACK_PROPERTY_H

#include <string>
#include <set>
#include <map>
#include <memory>
#include <functional>
#include <stdexcept>

#include "json11.hpp"

#include "gs_opencl.h"
#include "gs_solver.h"
#include "gs_variables.h"

namespace Geostack
{

    class PropertyMap;

    /**
    * %Property types
    */
    namespace PropertyType {
        enum Type {
            Undefined,     // Undefined
            String,        // String
            Integer,       // Signed int
            Float,         // Single precision float
            Double,        // Double precision float
            Index,         // Unsigned int
            Byte,          // Unsigned char
            FloatVector,   // Vector of single precision floats
            DoubleVector,  // Vector of double precision floats
            Map,           // Map
        };
    }

    namespace PropertyStructure {
        enum Type {
            Undefined,     // Undefined
            Scalar,        // Scalar
            Vector,        // Vector
        };
    }

    class PropertyBase {

    public:

        // Constructors
        PropertyBase() { }
        virtual ~PropertyBase() { }
        virtual PropertyType::Type getType() = 0;
        virtual PropertyStructure::Type getStructure() = 0;
        virtual std::string getOpenCLTypeString() = 0;
        virtual PropertyBase *clone() = 0;
        virtual const std::size_t getSize() = 0;
        virtual const cl::Buffer &getBuffer() = 0;
        virtual bool isBuffered() = 0;

        // Property data functions
        template <typename PTYPE>
        static PropertyType::Type getPropertyType(); ///< Get static type

        template <typename PTYPE>
        static PropertyStructure::Type getPropertyStructure(); ///< Get static structure

        template <typename PTYPE, typename RTYPE>
        static RTYPE convert(const PTYPE);
    };

    class PropertyUndefined : public PropertyBase {

    public:

        // Constructors
        PropertyUndefined() { }
        virtual ~PropertyUndefined() { }

        // Property data functions
        const cl::Buffer &getBuffer() {
            throw std::runtime_error("Cannot get buffer for undefined property");
        }

        bool isBuffered() {
            return false;
        }

        // Property information
        PropertyType::Type getType() {
            return PropertyType::Undefined;
        }
        PropertyStructure::Type getStructure() {
            return PropertyStructure::Undefined;
        }
        std::string getOpenCLTypeString() {
            throw std::runtime_error("Invalid OpenCL type");
        }
        const std::size_t getSize() {
            throw std::runtime_error("Cannot get size of undefined property");
        }

        // Clone data
        PropertyBase *clone() {
            return new PropertyUndefined;
        }
    };

    template <typename PTYPE>
    class Property : public PropertyBase {

    public:

        // Constructors
        Property() { clear(); }
        Property(PTYPE v_);
        Property(const Property &r);
        Property &operator=(const Property &r);
        virtual ~Property() { }

        // Property data functions
        template <typename RTYPE>
        RTYPE get() const;

        PTYPE &getRef() {
            return v;
        }

        const cl::Buffer &getBuffer() {
            throw std::runtime_error("Cannot get buffer for scalar property");
        }

        bool isBuffered() {
            return false;
        }

        template <typename RTYPE>
        void set(const RTYPE v);

        void clear();

        // Property information
        PropertyType::Type getType();
        std::string getOpenCLTypeString();
        PropertyStructure::Type getStructure() {
            return PropertyStructure::Scalar;
        }
        const std::size_t getSize() {
            throw std::runtime_error("Cannot get size of scalar property");
        }

        // Clone data
        PropertyBase *clone();

    private:

        // Data
        PTYPE v;

    };

    template <typename PTYPE>
    class PropertyVector : public PropertyBase {

    public:

        // Constructors
        PropertyVector() { }
        virtual ~PropertyVector() { }

        // Property information
        PropertyType::Type getType();        
        std::string getOpenCLTypeString();
        PropertyStructure::Type getStructure() {
            return PropertyStructure::Vector;
        }

        // Property data functions
        virtual PTYPE &getRef() = 0;
        
        // Get vector reduction
        virtual typename PTYPE::value_type reduce(Reduction::Type type) = 0;

    };

    template <typename PTYPE>
    class PropertyVectorUnbuffered : public PropertyVector<PTYPE> {

    public:

        // Constructors
        PropertyVectorUnbuffered() { }
        PropertyVectorUnbuffered(PTYPE v_);
        PropertyVectorUnbuffered(const PropertyVectorUnbuffered &r);
        PropertyVectorUnbuffered &operator=(const PropertyVectorUnbuffered &r);
        virtual ~PropertyVectorUnbuffered() { }

        // Property data functions
        PTYPE &getRef() {
            return v;
        }

        const cl::Buffer &getBuffer() {
            throw std::runtime_error("Cannot get buffer for unbuffered property");
        }

        bool isBuffered() {
            return false;
        }

        const std::size_t getSize() {
            return v.size();
        }

        void clear() {
            v.clear();
        }

        // Clone data
        PropertyBase *clone();

        // Get vector reduction
        typename PTYPE::value_type reduce(Reduction::Type type);

    private:

        // Data
        PTYPE v;
    };

    template <typename PTYPE>
    class PropertyVectorBuffered : public PropertyVector<PTYPE> {

    public:

        // Constructors
        PropertyVectorBuffered() { }
        PropertyVectorBuffered(PTYPE v_);
        PropertyVectorBuffered(const PropertyVectorBuffered &r);
        PropertyVectorBuffered &operator=(const PropertyVectorBuffered &r);
        virtual ~PropertyVectorBuffered() { }

        // Property data functions
        PTYPE &getRef() {
            return v.getData();
        }

        const cl::Buffer &getBuffer() {
            return v.getBuffer();
        }

        bool isBuffered() {
            return true;
        }

        void clear() {
            v.clear();
        }

        const std::size_t getSize() {
            return v.size();
        }

        // Clone data
        PropertyBase *clone();

        // Get vector reduction
        typename PTYPE::value_type reduce(Reduction::Type type);

    private:

        // Data
        VariablesVector<typename PTYPE::value_type> v;
    };

    /**
    * %PropertyMap class for geometry properties.
    * Holds a map of properties
    */
    class PropertyMap {

    public:

        // Constructors
        PropertyMap() { }
        PropertyMap(const PropertyMap &r);

        // Destructor
        virtual ~PropertyMap();

        // Assignment operator
        PropertyMap &operator=(const PropertyMap &r);

        // Addition operator
        PropertyMap &operator+=(const PropertyMap &r);

        // Add property
        void addProperty(std::string name);

        // Copy vector property
        void copy(std::string name, std::size_t from, std::size_t to);

        // Set property
        template <typename PTYPE>
        void setProperty(std::string name, PTYPE v);

        // Get all properties of certain type.
        template <typename PTYPE>
        std::map<std::string, PTYPE> getProperties() const;

        // Get undefined properties.
        std::set<std::string> getUndefinedProperties() const;

        // Get property with conversion
        template <typename PTYPE>
        PTYPE getProperty(std::string name) const;

        template <typename PTYPE>
        PTYPE getPropertyFromVector(std::string name, std::size_t index) const;

        template <typename PTYPE>
        PTYPE reductionFromVector(std::string name, Reduction::Type type) const;

        // Get property reference
        template<typename PTYPE>
        PTYPE &getPropertyRef(std::string name);
        
        bool hasPropertyBuffer(std::string name);
        cl::Buffer const &getPropertyBuffer(std::string name);

        // Get all property references of certain type.
        template <typename PTYPE>
        std::map<std::string, std::reference_wrapper<PTYPE> > getPropertyRefs() const;

        // Get property type
        PropertyType::Type getPropertyType(std::string name) const;
                
        // Get property name
        std::string getOpenCLTypeString(std::string name) const;

        // Get property stucture
        PropertyStructure::Type getPropertyStructure(std::string name) const;

        // Remove property
        void removeProperty(std::string name);

        // Convert property vector
        template <typename PTYPE>
        void convertProperty(std::string name);

        // Clear all properties
        void clear();

        // Resize all vectors
        void resize(std::size_t size);

        // Get property buffer size
        std::size_t getSize(std::string name) const;

        // Get names
        std::set<std::string> getPropertyNames() const;
        std::set<std::string> getPropertyVectorNames() const;

        /**
        * Check for %Property.
        * @return true if property name is found, false otherwise
        */
        bool hasProperty(std::string name) const {
            return (properties.find(name) != properties.end());
        }

        // Write properties to Json    
        json11::Json toJson();
        std::string toJsonString();

    protected:

        template <typename PTYPE>
        PTYPE &getPropertyRefScalar(std::string name);

        template <typename PTYPE>
        PTYPE &getPropertyRefVectorUnbuffered(std::string name);

        template <typename PTYPE>
        PTYPE &getPropertyRefVectorBuffered(std::string name);

        std::map<std::string, PropertyBase *> properties; ///< Map of named properties
    };
}

#endif
