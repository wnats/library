/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_ARGS_H
#define GEOSTACK_ARGS_H

#include <map>
#include <vector>
#include <string>
 
namespace Geostack
{
namespace Args
{
    // Command line argument parser
    std::map<std::string, std::vector<std::string> > parse(int argc, char **argv);
}
}

#endif