/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*__RK_COEFF__*/

/**
* Custom functions
*/
/*__CODE__*/

/**
* ODE update operation
* @param dt timestep
* @param _n number of equations
* @param _N size of equation set
*/
__kernel void solve(
    __global REAL *_vars,    
    const REAL t_initial,
    const REAL dt_initial,
    const uint _n,
    const uint _N/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);

    // Check limits
    if (index < _N) {
    
        // Get variables
/*__VARS__*/

        // Adaptive Runge-Kutta (first-same-as-last)
        int nsteps = 0;
        REAL dt = dt_initial;
        REAL error = 0.0;

        do {
        
            // Initialise variables
            REAL t = t_initial;
/*__INIT__*/

            for (int c = 0; c < (1<<nsteps); c++) {

                // Reset error
                error = 0.0;

/*__RK__*/

                if (error > __TOLERANCE__ && nsteps < __MAX_STEPS__)
                    break;

                // Update
                t += dt;
/*__UPDATE__*/
            }

            // Update timestep
            dt*=0.5;
            nsteps++;

        } while(error > __TOLERANCE__ && nsteps <= __MAX_STEPS__);

        // ---------------------------------
        // User defined code
/*__POST1__*/
        // ---------------------------------

        // Store variables
/*__POST2__*/

    }
}
