/**
* Indexed 3D raster operator
* Calculations are run over a set of indexes defined in _ri
*/
typedef struct RasterIndexStruct {

    // Coordinates
    uint id;
    ushort i;
    ushort j;
    REAL w;
    
} __attribute__ ((aligned (8))) RasterIndex;

/**
* Raster value calculation from indexes
*/
__kernel void rasterIndexed(
    __global RasterIndex *_ri,
    const uint _n/*__ARGS2__*/
/*__ARGS__*/
) {
        
    // Get index
    const size_t _index = get_global_id(0);
    const size_t _layers = get_global_size(1);
    if (_index < _n) {

        uint _i = (uint)(_ri+_index)->i;
        uint _j = (uint)(_ri+_index)->j;
        uint _k = get_global_id(1);

        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/
/*__VARS2__*/

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

/*__POST__*/
/*__POST2__*/

    }
}

