/**
* Shallow water streaming step
*/
__kernel void streaming(
REAL c,
REAL tau,
REAL drag_dt,
__global REAL *_h,
__global REAL *_uh,
__global REAL *_vh,
__global REAL *__fi,
__global REAL *__fe,
__global REAL *__fe_N,
__global REAL *__fe_NE,
__global REAL *__fe_E,
__global REAL *__fe_SE,
__global REAL *__fe_S,
__global REAL *__fe_SW,
__global REAL *__fe_W,
__global REAL *__fe_NW,
__global REAL *__ft,
__global REAL *__ft_N,
__global REAL *__ft_NE,
__global REAL *__ft_E,
__global REAL *__ft_SE,
__global REAL *__ft_S,
__global REAL *__ft_SW,
__global REAL *__ft_W,
__global REAL *__ft_NW,
const Dimensions _dim,
uchar _isBoundaryTile
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

/*__VARS__*/

        const bool _boundary_W = (_i == 0);
        const bool _boundary_S = (_j == 0);
        const bool _boundary_E = (_i == _dim.mx-1);
        const bool _boundary_N = (_j == _dim.my-1);

        REAL _fi_new[9];

        // Lattice stencil and indexes
        //
        // NW (8)  N (4)  NE (7)
        //       \   |   /
        //        \  |  /
        //         \ | /
        //          \|/
        // W (1) ----0---- E (3)
        //          /|\
        //         / | \
        //        /  |  \
        //       /   |   \
        // SW (5)  S (2)  SE (6)
        //

        // Stream
        _fi_new[0] = _val3D_a(__ft, _i, _j, 0);
        _fi_new[1] = _val3D_a_W(__ft, _i, _j, 1);
        _fi_new[2] = _val3D_a_S(__ft, _i, _j, 2);
        _fi_new[3] = _val3D_a_E(__ft, _i, _j, 3);
        _fi_new[4] = _val3D_a_N(__ft, _i, _j, 4);
        _fi_new[5] = _val3D_a_SW(__ft, _i, _j, 5);
        _fi_new[6] = _val3D_a_SE(__ft, _i, _j, 6);
        _fi_new[7] = _val3D_a_NE(__ft, _i, _j, 7);
        _fi_new[8] = _val3D_a_NW(__ft, _i, _j, 8);
        
        REAL fnew0, fnew1;

        if (h == 0) {
        
            // Dry cells
            // Treatment based on Liu and Zhou, J. Fluid Mech. (2014), vol. 743, pp 32-59

            fnew0 = _fi_new[3];
            fnew1 = _fi_new[1];
            if (_fi_new[1] > 0.0 && isInvalid_REAL(_fi_new[3])) {
                fnew0 = -tau*_val3D_a_W(__fe, _i, _j, 3);
            } 
            if (_fi_new[3] > 0.0 && isInvalid_REAL(_fi_new[1])) {
                fnew1 = -tau*_val3D_a_E(__fe, _i, _j, 1);
            }
            _fi_new[3] = fnew0;
            _fi_new[1] = fnew1;
            
            fnew0 = _fi_new[4];
            fnew1 = _fi_new[2];
            if (_fi_new[2] > 0.0 && isInvalid_REAL(_fi_new[4])) {
                fnew0 = -tau*_val3D_a_S(__fe, _i, _j, 4);
            }
            if (_fi_new[4] > 0.0 && isInvalid_REAL(_fi_new[2])) {
                fnew1 = -tau*_val3D_a_N(__fe, _i, _j, 2);
            }
            _fi_new[4] = fnew0;
            _fi_new[2] = fnew1;
            
            fnew0 = _fi_new[7];
            fnew1 = _fi_new[5];
            if (_fi_new[5] > 0.0 && isInvalid_REAL(_fi_new[7])) {
                fnew0 = -tau*_val3D_a_SW(__fe, _i, _j, 7);
            }
            if (_fi_new[7] > 0.0 && isInvalid_REAL(_fi_new[5])) {
                fnew1 = -tau*_val3D_a_NE(__fe, _i, _j, 5);
            }
            _fi_new[7] = fnew0;
            _fi_new[5] = fnew1;
            
            fnew0 = _fi_new[8];
            fnew1 = _fi_new[6];
            if (_fi_new[6] > 0.0 && isInvalid_REAL(_fi_new[8])) {
                fnew0 = -tau*_val3D_a_SE(__fe, _i, _j, 8);
            }
            if (_fi_new[8] > 0.0 && isInvalid_REAL(_fi_new[6])) {
                fnew1 = -tau*_val3D_a_NW(__fe, _i, _j, 6);
            }
            _fi_new[8] = fnew0;
            _fi_new[6] = fnew1;

            // Fill in missing cells
            for (int k = 0; k < 9; k++) {
                if (isInvalid_REAL(_fi_new[k])) {
                    _fi_new[k] = 0.0;
                }
            }

        } else {

            // Wet cells
            //   If the neighbouring cell is dry and the wet cell has a component into
            //   the dry cell set the component, otherwise apply bounce-back

            if (isInvalid_REAL(_fi_new[1]) && isInvalid_REAL(_fi_new[3])) {
                _fi_new[1] = _val3D_a(__fi, _i, _j, 3);
                _fi_new[3] = _val3D_a(__fi, _i, _j, 1);
            } else {
            
                fnew0 = _fi_new[3];
                fnew1 = _fi_new[1];
                if (isInvalid_REAL(_fi_new[3])) {

                    if (!isBoundary_E && _val3D_a(__fi, _i, _j, 1) > 0.0) {
                        fnew0 = tau*_val3D_a(__fe, _i, _j, 3);
                    } else {
                        fnew0 = _fi_new[1];
                    }
                }
                if (isInvalid_REAL(_fi_new[1])) {

                    if (!isBoundary_W && _val3D_a(__fi, _i, _j, 3) > 0.0) {
                        fnew1 = tau*_val3D_a(__fe, _i, _j, 1);
                    } else {
                        fnew1 = _fi_new[3];
                    }
                } 
                _fi_new[3] = fnew0;
                _fi_new[1] = fnew1;
            }

            if (isInvalid_REAL(_fi_new[2]) && isInvalid_REAL(_fi_new[4])) {
                _fi_new[2] = _val3D_a(__fi, _i, _j, 4);
                _fi_new[4] = _val3D_a(__fi, _i, _j, 2);
            } else {
            
                fnew0 = _fi_new[4];
                fnew1 = _fi_new[2];
                if (isInvalid_REAL(_fi_new[4])) {

                    if (!isBoundary_N && _val3D_a(__fi, _i, _j, 2) > 0.0) {
                        fnew0 = tau*_val3D_a(__fe, _i, _j, 4);
                    } else {
                        fnew0 = _fi_new[2];
                    }
                }
                if (isInvalid_REAL(_fi_new[2])) {

                    if (!isBoundary_S && _val3D_a(__fi, _i, _j, 4) > 0.0) {
                        fnew1 = tau*_val3D_a(__fe, _i, _j, 2);
                    } else {
                        fnew1 = _fi_new[4];
                    }
                }
                _fi_new[4] = fnew0;
                _fi_new[2] = fnew1;
            }

            if (isInvalid_REAL(_fi_new[5]) && isInvalid_REAL(_fi_new[7])) {
                _fi_new[5] = _val3D_a(__fi, _i, _j, 7);
                _fi_new[7] = _val3D_a(__fi, _i, _j, 5);
            } else {
            
                fnew0 = _fi_new[7];
                fnew1 = _fi_new[5];            
                if (isInvalid_REAL(_fi_new[7])) {

                    if (!isBoundary_N && !isBoundary_E && _val3D_a(__fi, _i, _j, 5) > 0.0) {
                        fnew0 = tau*_val3D_a(__fe, _i, _j, 7);
                    } else {
                        fnew0 = _fi_new[5];
                    }
                }
                if (isInvalid_REAL(_fi_new[5])) {

                    if (!isBoundary_S && !isBoundary_W && _val3D_a(__fi, _i, _j, 7) > 0.0) {
                        fnew1 = tau*_val3D_a(__fe, _i, _j, 5);
                    } else {
                        fnew1 = _fi_new[7];
                    }
                } 
                _fi_new[7] = fnew0;
                _fi_new[5] = fnew1;
            }

            if (isInvalid_REAL(_fi_new[6]) && isInvalid_REAL(_fi_new[8])) {
                _fi_new[6] = _val3D_a(__fi, _i, _j, 8);
                _fi_new[8] = _val3D_a(__fi, _i, _j, 6);
            } else {
            
                fnew0 = _fi_new[8];
                fnew1 = _fi_new[6];
                if (isInvalid_REAL(_fi_new[8])) {

                    if (!isBoundary_N && !isBoundary_W && _val3D_a(__fi, _i, _j, 6) > 0.0) {
                        fnew0 = tau*_val3D_a(__fe, _i, _j, 8);
                    } else {
                        fnew0 = _fi_new[6];
                    }
                }
                if (isInvalid_REAL(_fi_new[6])) {

                    if (!isBoundary_S && !isBoundary_E && _val3D_a(__fi, _i, _j, 8) > 0.0) {
                        fnew1 = tau*_val3D_a(__fe, _i, _j, 6);
                    } else {
                        fnew1 = _fi_new[8];
                    }
                }
                _fi_new[8] = fnew0;
                _fi_new[6] = fnew1;
            }
        }
    
        // Calculate new height
        REAL h_last = h;
        h = _fi_new[0]+_fi_new[1]+_fi_new[2]+_fi_new[3]+_fi_new[4]+_fi_new[5]+_fi_new[6]+_fi_new[7]+_fi_new[8];

        if (h > 0.0) {

            // Store in global memory
            for (int k = 0; k < 9; k++) {
                _val3D_a(__fi, _i, _j, k) = _fi_new[k];
            }
    
            // Calculate unit discharge
            uh = c*(_fi_new[1]-_fi_new[3]+_fi_new[5]-_fi_new[6]-_fi_new[7]+_fi_new[8]);
            vh = c*(_fi_new[2]-_fi_new[4]+_fi_new[5]+_fi_new[6]-_fi_new[7]-_fi_new[8]);
            
            // Apply drag, this is the solution of dq/dt = -g n_m^2 q^2 h^(-7/3)
            REAL h_term = pow_DEF(0.5*(h+h_last), 7.0/3.0);
            REAL f_term = h_term/(h_term+drag_dt*hypot(uh, vh));

            // Calculate unit discharge with drag
            uh *= f_term;
            vh *= f_term;

        } else {
    
            // Set all variables to zero 
            h = 0.0;
            uh = 0.0;
            vh = 0.0;
            
            for (int k = 0; k < 9; k++) {
                _val3D_a(__fi, _i, _j, k) = 0.0;
            }
        }

/*__POST__*/

    }
}

