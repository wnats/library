/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#include <iostream>
#include <sstream>

#include "json11.hpp"
#include "base64.hpp"
#include "miniz.h"

#include "gs_raster_json.h"

using namespace json11;

namespace Geostack
{
    /**
    * Write Raster to Json string
    * @param compress deflate data vector.
    * @return Json string with header and data.
    */
    template <typename RTYPE, typename CTYPE>
    std::string JsonHandler<RTYPE, CTYPE>::toJson(Raster<RTYPE, CTYPE> &r, bool compress) {
            
        Json::object jsonRoot;

        // Set name        
        jsonRoot["name"] = r.template getProperty<std::string>("name");
        
        // Set dimensions
        auto dims = r.getRasterDimensions();
        jsonRoot["nx"] = (int)dims.d.nx;
        jsonRoot["ny"] = (int)dims.d.ny;
        jsonRoot["nz"] = (int)1;
        jsonRoot["hx"] = dims.d.hx;
        jsonRoot["hy"] = dims.d.hy;
        jsonRoot["hz"] = dims.d.hz;
        jsonRoot["ox"] = dims.d.ox;
        jsonRoot["oy"] = dims.d.oy;
        jsonRoot["oz"] = dims.d.oz;

        // Set projection
        jsonRoot["proj4"] = Projection::toPROJ4(r.getProjectionParameters());
        
        // Set endianess
        #if defined(ENDIAN_LITTLE)
        jsonRoot["endian"] = "little";
        #else
        jsonRoot["endian"] = "big";
        #endif

        // Set bounds
        auto boundNW = Coordinate<double>( { dims.d.ox, dims.ey } );
        auto boundNE = Coordinate<double>( { dims.ex, dims.ey } );
        auto boundSE = Coordinate<double>( { dims.ex, dims.d.oy } );
        auto boundSW = Coordinate<double>( { dims.d.ox, dims.d.oy } );
        
        // Convert bounds to EPSG4326
        auto proj = r.getProjectionParameters();
        ProjectionParameters<double> proj_EPSG4326 = Projection::parsePROJ4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
        Projection::convert(boundNW, proj_EPSG4326, proj);
        Projection::convert(boundNE, proj_EPSG4326, proj);
        Projection::convert(boundSE, proj_EPSG4326, proj);
        Projection::convert(boundSW, proj_EPSG4326, proj);

        jsonRoot["bounds"] = Json::array( {
            Json::array( { boundNW.p, boundNW.q } ),
            Json::array( { boundNE.p, boundNE.q } ),
            Json::array( { boundSE.p, boundSE.q } ),
            Json::array( { boundSW.p, boundSW.q } )
        } );

        // Unpack all data
        uint64_t k = 0; // TODO, write multiple layers
        uint64_t koff = (uint64_t)k*TileMetrics::tileSizeSquared;
        
        std::vector<RTYPE> data;
        std::vector<std::vector<RTYPE> > dataCache;
        dataCache.resize(dims.tx);
        for (int32_t tj = 0; tj < dims.ty; tj++) {

            // Populate cache
            for (uint32_t ti = 0; ti < dims.tx; ti++) {
                r.getTileData(ti, dims.ty-1-tj, dataCache[ti]);
            }
            
            // Unpack data
            uint32_t rowEnd = dims.d.nx&TileMetrics::tileSizeMask;
            uint32_t nj = tj == 0 ? TileMetrics::tileSize*(1-dims.ty)+dims.d.ny : TileMetrics::tileSize;
            for (uint32_t j = 0; j < nj; j++) {

                // Calculate offset
                uint64_t joff = ((uint64_t)(nj-j-1)<<TileMetrics::tileSizePower)+koff;

                // Write lines
                for (uint32_t ti = 0; ti < dims.tx-1; ti++) {
                    auto it = dataCache[ti].begin()+joff;
                    data.insert(data.end(), it, it+TileMetrics::tileSize);
                }
                auto it = dataCache[dims.tx-1].begin()+joff;
                data.insert(data.end(), it, it+rowEnd);
            }
        }        

        std::vector<unsigned char> cData;
        if (compress) {

            // Set data sizes
            uint64_t dataLength = data.size()*sizeof(RTYPE);
            mz_ulong compressedDataLen = compressBound((mz_ulong)dataLength);

            // Compress
            cData.resize(compressedDataLen);
            mz_ulong len = compressedDataLen;
            int status = compress2(cData.data(), &len, 
                reinterpret_cast<const unsigned char *>(data.data()), (mz_ulong)dataLength, MZ_BEST_SPEED);

            if (status != Z_OK) {
                std::stringstream err;
                err << "Compression failed '" << mz_error(status) << "'";
                throw std::runtime_error(err.str());
            }

            // Shrink vector
            cData.resize(len);
            cData.shrink_to_fit();

            jsonRoot["zip"] = true;

        } else {

            unsigned char *pData = reinterpret_cast<unsigned char *>(data.data());
            cData = std::vector<unsigned char>(pData, pData+data.size()*sizeof(RTYPE));

            jsonRoot["zip"] = false;
        }

        // Convert to b64
        jsonRoot["data"] = base64::to_base64(cData);

        Json json = jsonRoot;
        return json.dump();
    }

    /**
    * Read Json to raster.
    * @param fileName input file name.
    * @param r input Raster.
    * @param jsonConfig write configuration.
    */
    template <typename RTYPE, typename CTYPE>
    void JsonHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // TODO
        throw std::runtime_error("Raster JSON read functions are not yet available");
    }

    /**
    * Write Json from raster.
    * @param fileName output file name.
    * @param r input Raster.
    * @param jsonConfig write configuration.
    */
    template <typename RTYPE, typename CTYPE>
    void JsonHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            throw std::runtime_error("Raster has no data to write to json file");
        }

        // Write data
        std::ofstream out(fileName);
        out << toJson(r, true);
    }

    // Type definitions
    template class JsonHandler<float, float>;
    template class JsonHandler<uint8_t, float>;
    template class JsonHandler<uint32_t, float>;
    // template std::string JsonHandler<float, float>::toJson(Raster<float, float> &, bool);
    // template std::string JsonHandler<uint32_t, float>::toJson(Raster<uint32_t, float> &, bool);
    // template std::string JsonHandler<uint8_t, float>::toJson(Raster<uint8_t, float> &, bool);
    
    template class JsonHandler<double, double>;
    template class JsonHandler<uint8_t, double>;
    template class JsonHandler<uint32_t, double>;
    // template std::string JsonHandler<double, double>::toJson(Raster<double, double> &, bool);
    // template std::string JsonHandler<uint32_t, double>::toJson(Raster<uint32_t, double> &, bool);
    // template std::string JsonHandler<uint8_t, double>::toJson(Raster<uint8_t, double> &, bool); 
}