/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <algorithm>
#include <sstream>
#include <numeric>

#include "gs_solver.h"
#include "gs_string.h"
#include "gs_series.h"

namespace Geostack
{
    /**
     * Series constructor
     */
    template <typename XTYPE, typename YTYPE>
    Series<XTYPE, YTYPE>::Series(): 
        name(), 
        interpolation(SeriesInterpolation::Linear),
        capping(SeriesCapping::Uncapped) {

        // Reset values
        clear();
    }

    /**
     * Series constructor with name
     */
    template <typename XTYPE, typename YTYPE>
    Series<XTYPE, YTYPE>::Series(std::string _name): 
        name(_name), 
        interpolation(SeriesInterpolation::Linear),
        capping(SeriesCapping::Uncapped) {

        // Reset values
        clear();
    }

    /**
     * Series copy constructor
     * @param r %Series to copy.
     */
    template <typename XTYPE, typename YTYPE>
    Series<XTYPE, YTYPE>::Series(const Series<XTYPE, YTYPE> &r):
        name(r.name), 
        interpolation(r.interpolation), 
        capping(r.capping),
        values(r.values), 
        slopes(r.slopes),
        x_max(r.x_max), 
        x_min(r.x_min), 
        y_max(r.y_max), 
        y_min(r.y_min),
        y_upperBound(r.y_upperBound), 
        y_lowerBound(r.y_lowerBound) { }

    /**
     * Series assignment operator
     * @param r %Series to copy.
     */
    template <typename XTYPE, typename YTYPE>
    Series<XTYPE, YTYPE> &Series<XTYPE, YTYPE>::operator=(const Series<XTYPE, YTYPE> &r) {

        if (this != &r) {
        
            // Copy data
            name = r.name;
            interpolation = r.interpolation;
            capping = r.capping;
            values = r.values;
            slopes = r.slopes;

            x_max = r.x_max;
            x_min = r.x_min;
            y_max = r.y_max; 
            y_min = r.y_min;

            y_upperBound = r.y_upperBound; 
            y_lowerBound = r.y_lowerBound;
        }
        return *this;
    }

    /**
     * Clear series
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::clear() {

        // Clear all vectors
        values.clear();
        slopes.clear();

        // Update limits
        x_max = getNullValue<XTYPE>();
        x_min = getNullValue<XTYPE>();
        y_max = getNullValue<YTYPE>();
        y_min = getNullValue<YTYPE>();

        // Update bounds
        y_upperBound = getNullValue<YTYPE>();
        y_lowerBound = getNullValue<YTYPE>();
    }

    /**
     * Adds a value to a series and re-calculates gradients.
     * @param y ordinate value
     * @param x abscissa value
     * @param isSorted flag to specify if value is sorted
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::addValue(XTYPE x, YTYPE y, bool isSorted) {

        values.push_back( { x, y } );
        update(isSorted);
    }

    /**
     * Adds a value to a series and re-calculates gradients.
     * @param y ordinate value
     * @param x string abscissa value
     * @param isSorted flag to specify if value is sorted
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::addValue(std::string x, YTYPE y, bool isSorted) {
    
        // Try to convert from numeric string
        if (Strings::isNumber(x)) {
            values.push_back( { Strings::toNumber<XTYPE>(x), y } );
        } else {

            // Try to convert from ISO8601 date
            auto timeSinceEpoch = Strings::iso8601toEpoch(x);
            if (timeSinceEpoch != getNullValue<int64_t>() ) {
                values.push_back( { (XTYPE)timeSinceEpoch, y } );
            } else {
                std::stringstream err;
                err << "Invalid ISO8601 string (time zone must be included) '" << x << "'";
                throw std::runtime_error(err.str());
            }
        }
        update(isSorted);
    }

    /**
     * Adds a vector of values to a series and re-calculates gradients.
     * @param newValues vector of %SeriesPair values
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::addValues(const std::vector<SeriesItem<XTYPE, YTYPE> > newValues) {

        values.insert(values.end(), newValues.begin(), newValues.end());
        update();
    }

    /**
     * Adds a vector of string and value pairs to a series and re-calculates gradients.
     * @param newValues vector of %SeriesPair values
     */
    template <typename XTYPE, typename YTYPE>
    bool Series<XTYPE, YTYPE>::addValues(const std::vector<std::pair<std::string, YTYPE> > newValues) {

        // Parse values
        for (auto &value : newValues) {

            // Try to convert from numeric string
            if (Strings::isNumber(value.first)) {
                values.push_back( { Strings::toNumber<XTYPE>(value.first), value.second } );
            } else {

                // Try to convert from ISO8601 date
                auto timeSinceEpoch = Strings::iso8601toEpoch(value.first);
                if (timeSinceEpoch == getNullValue<int64_t>() ) {
                    return false;
                } else {
                    values.push_back( { (XTYPE)timeSinceEpoch, value.second } );
                }
            }
        }

        // Update series
        update();
        return true;
    }

    /**
     * Calculate gradients for monotone cubic Hermite interpolation. Derivatives are limited using Fritsch-Carlson method.
     * @param isSorted flag to specify if value is sorted
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::update(bool isSorted) {
    
        // Limit derivatives using Fritsch-Carlson method
        // See: http://math.stackexchange.com/questions/45218/implementation-of-monotone-cubic-interpolation
        // and: http://dx.doi.org/10.1137/0717021  
        
        if (!isSorted) {

            // Sort values, using <= to ensure last added duplicates are pushed to front
            std::sort(values.begin(), values.end(), 
                [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x <= v1.x; });
            
            // Remove duplicate values
            auto vit = std::unique(values.begin(), values.end(), 
                [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x == v1.x; });
            values.erase(vit, values.end());
        }

        // Update limits
        updateLimits();

        // Calculate the tangents for each point
        std::size_t n = values.size();
        if (n == 0)
            return;
        slopes.resize(n);

        if (n == 1) {

            // Case for n = 1
            slopes[0] = 0.0;

        } else if (n == 2) {

            // Case for n = 2
            double h0 = (double)(values[1].x-values[0].x);
            double d0 = (double)(values[1].y-values[0].y)/h0;
            slopes[0] = d0;
            slopes[1] = d0;

        } else {
        
            // Case for n > 2
            if (isSorted) {
            
                // Calculate derivatives
                double h0 = (double)(values[n-2].x-values[n-3].x);
                double d0 = (double)(values[n-2].y-values[n-3].y)/h0;
                double h1 = (double)(values[n-1].x-values[n-2].x);
                double d1 = (double)(values[n-1].y-values[n-2].y)/h1;
                
                // Limit derivatives using Fritsch-Carlson method
                if (d0*d1 < 0.0) {
                    slopes[n-2] = 0.0;
                } else {
                    slopes[n-2] = 3.0*(h0+h1)/(((2.0*h1+h0)/d0)+((h1+2.0*h0)/d1));
                }
                slopes[n-1] = d1;

            } else {

                // Calculate derivatives
                std::vector<double> d(n-1);
                std::vector<double> h(n-1);
                for (int i = 0; i < n-1; i++) {
                    h[i] = (double)(values[i+1].x-values[i].x);
                    d[i] = (double)(values[i+1].y-values[i].y)/h[i];
                }

                // Calculate tangents
                slopes[0] = d[0];
                slopes[n-1] = d[n-2];
                for (std::size_t i = 1; i < n-1; i++) {
                    slopes[i] = 0.5*(d[i-1]+d[i]);
                }

                // Limit derivatives using Fritsch-Carlson method
                for (std::size_t i = 1; i < n-1; i++) {

                    // Compare derivative signs
                    if (d[i]*d[i-1] < 0.0) {
                        slopes[i] = 0.0;
                    } else {
                        slopes[i] = 3.0*(h[i-1]+h[i])/(((2.0*h[i]+h[i-1])/d[i-1])+((h[i]+2.0*h[i-1])/d[i]));
                    }
                }
            }
        }
        
    }

    /**
     * Find maximum and minimum values in series.
     */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::updateLimits() {
    
        // x-values are sorted
        x_min = values[0].x;
        x_max = values[values.size()-1].x;

        // y-values are unsorted
        y_min = values[0].y;
        y_max = values[0].y;
        for (int i = 1; i < values.size(); i++) {
            YTYPE &y = values[i].y;
            if (y < y_min) y_min = y;
            if (y > y_max) y_max = y;
        }
    }
        
    /**
     * Find if a value is within the range of a series.
     * @param x abscissa value to check.
     * @return \b true if within range, \b false otherwise.
     */
    template <typename XTYPE, typename YTYPE>
    bool Series<XTYPE, YTYPE>::inRange(const XTYPE x) {
    
        // Return false for null series
        if (values.size() == 0)
            return false;

        // Return true for constant series
        if (values.size() == 1)
            return true;

        // Check bounds
        if (x < x_min || x > x_max)
            return false;
        
        return true;
    }

    /**
    * Set bounds for bounded interpolation
    */
    template <typename XTYPE, typename YTYPE>
    void Series<XTYPE, YTYPE>::setBounds(YTYPE y_firstBound, YTYPE y_secondBound) { 
        y_upperBound = std::max(y_firstBound, y_secondBound);
        y_lowerBound = std::min(y_firstBound, y_secondBound);
    }

    /**
     * Linear series interpolation.
     * @param x abscissa value.
     * @return linear interpolation of series at point <b>x</b>.
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::getLinear(XTYPE x) {

        // Check range
        if (!inRange(x)) {
            if (capping == SeriesCapping::Uncapped) {
                return getNullValue<YTYPE>();
            } else {             
                if (x < x_min) {
                    return values.front().y;
                } else {
                    return values.back().y;
                }
            }
        }
        
        // Find lower index around x
        auto ilo = std::lower_bound(values.begin(), values.end(), SeriesItem<XTYPE, YTYPE>( { x, (YTYPE)0 } ), 
            [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x < v1.x; });
        std::size_t lo = ilo-values.begin();
        if (lo > 0) lo--;

        // Linear interpolation
        double iVal = (double)(values[lo+1].y-values[lo].y)*(double)(x-values[lo].x)/(double)(values[lo+1].x-values[lo].x);
        return values[lo].y+(YTYPE)iVal;
    }

    /**
     * Monotone cubic series interpolation.
     * @param x abscissa value.
     * @return monotone cubic interpolation of series at point <b>x</b>.
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::getMonotoneCubic(XTYPE x) {

        // Check range
        if (!inRange(x)) {
            if (capping == SeriesCapping::Uncapped) {
                return getNullValue<YTYPE>();
            } else {             
                if (x < x_min) {
                    return values.front().y;
                } else {
                    return values.back().y;
                }
            }
        }
        
        // Find lower index around x
        auto ilo = std::lower_bound(values.begin(), values.end(), SeriesItem<XTYPE, YTYPE>( { x, (YTYPE)0 } ), 
            [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x < v1.x; });
        std::size_t lo = ilo-values.begin();
        if (lo > 0) lo--;

        // Cubic spline interpolation
        // See: http://math.stackexchange.com/questions/4082/equation-of-a-curve-given-3-points-and-additional-constant-requirements#4104
        double idx = 1.0/(double)(values[lo+1].x-values[lo].x);
        double s = (double)(values[lo+1].y-values[lo].y)*idx;
        double c = (3.0*s-2.0*slopes[lo]-slopes[lo+1])*idx;
        double d = (slopes[lo]+slopes[lo+1]-2.0*s)*(idx*idx);

        // Cubic interpolation
        double dx = (double)(x-values[lo].x);
        double iVal = (slopes[lo]+(c+d*dx)*dx)*dx;
        return values[lo].y+(YTYPE)iVal;
    }

    /**
     * Bounded linear series interpolation.
     * @param x abscissa value.
     * @return linear interpolation of series at point <b>x</b> bounded by upper and lower bounds.
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::getBoundedLinear(XTYPE x) {        

        // Check range
        if (!inRange(x)) {
            if (capping == SeriesCapping::Uncapped) {
                return getNullValue<YTYPE>();
            } else {             
                if (x < x_min) {
                    return values.front().y;
                } else {
                    return values.back().y;
                }
            }
        }

        // Check bounds
        if (y_upperBound != y_upperBound) {
            y_upperBound = y_max;
        }
        if (y_lowerBound != y_lowerBound) {
            y_lowerBound = y_min;
        }
        
        // Find lower index around x
        auto ilo = std::lower_bound(values.begin(), values.end(), SeriesItem<XTYPE, YTYPE>( { x, (YTYPE)0 } ), 
            [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x < v1.x; });
        std::size_t lo = ilo-values.begin();
        if (lo > 0) lo--;

        // Bounded linear interpolation
        YTYPE y_0 = values[lo].y;
        YTYPE y_1 = values[lo+1].y;

        double delta_y = 0.0;
        if (y_0 > y_1) {

            double step_A = (double)(y_0-y_1);
            double step_B = (double)(y_1-y_lowerBound)+(double)(y_upperBound-y_0);
            delta_y = step_A < step_B ? -step_A : step_B;
        } else if (y_0 < y_1) {

            double step_A = (double)(y_1-y_0);
            double step_B = (double)(y_0-y_lowerBound)+(double)(y_upperBound-y_1);
            delta_y = step_A < step_B ? step_A : -step_B;
        }

        double delta_x = (double)(values[lo+1].x-values[lo].x);
        double iVal = (double)(x-values[lo].x)*delta_y/delta_x;
        YTYPE rVal = y_0+(YTYPE)iVal;

        if (rVal < y_lowerBound) 
            rVal+=(y_upperBound-y_lowerBound);
        else if (rVal > y_upperBound) 
            rVal-=(y_upperBound-y_lowerBound);

        return rVal;
    }

    /**
     * Series interpolation.
     * @param x abscissa value.
     * @return constant value for constant series or monotone cubic interpolation of series at point <b>x</b>.
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::operator()(const XTYPE x) {
    
        // Return first value for constant series
        if (values.size() == 1)
            return values[0].y;

        // Return interpolated value
        switch (interpolation) {
            case SeriesInterpolation::Linear:
                return getLinear(x);

            case SeriesInterpolation::MonotoneCubic:
                return getMonotoneCubic(x);  

            case SeriesInterpolation::BoundedLinear:
                return getBoundedLinear(x);  
        }

        // Return no-data value
        return getNullValue<YTYPE>();
    }

    /**
     * Series interpolation on a vector of abscissa values
     * @param x vector of abscissa values.
     * @return y vector of values at x vector
     */
    template <typename XTYPE, typename YTYPE>
    std::vector<YTYPE> Series<XTYPE, YTYPE>::operator()(const std::vector<XTYPE> vx) {

      // Loop through the abscissa values
      std::vector<YTYPE> out;
      for (auto &x : vx) {
        YTYPE val = (*this)(x);
        out.push_back(val);
      }
      return out;
    }

    /**
     * Get abscissa values
     */
    template <typename XTYPE, typename YTYPE>
    std::vector<XTYPE> Series<XTYPE, YTYPE>::getAbscissas(){
      std::vector<XTYPE> out;
      for (auto &v : values) {
        out.push_back(v.x);
      }
      return out;
    }

    /**
     * Get ordinates
     */
    template <typename XTYPE, typename YTYPE>
    std::vector<YTYPE> Series<XTYPE, YTYPE>::getOrdinates(){
      std::vector<YTYPE> out;
      for (auto &v : values) {
        out.push_back(v.y);
      }
      return out;
    }

    /**
     * @brief check if the abscissa are sorted
     * 
     * @return true if sorted 
     * @return false otherwise
     */
    template <typename XTYPE, typename YTYPE>
    bool Series<XTYPE, YTYPE>::isSorted(){
        bool out = false;
        out = std::is_sorted(values.begin(), values.end(), 
            [](const SeriesItem<XTYPE, YTYPE> &v0, const SeriesItem<XTYPE, YTYPE> &v1) { return v0.x < v1.x; });
        return out;
    }

    /**
     * @brief get sum of ordinates 
     * 
     * @return YTYPE total values of ordinates
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::sum(){
        auto v = std::accumulate(values.begin(), values.end(), (YTYPE)0,
            [](const YTYPE &v1, const SeriesItem<XTYPE, YTYPE> &v0) {return v1 + v0.y;});
        return v;
    }

    /**
     * @brief get mean value of ordinates
     * 
     * @return YTYPE mean value of ordinates
     */
    template <typename XTYPE, typename YTYPE>
    YTYPE Series<XTYPE, YTYPE>::mean(){
        YTYPE v = getNullValue<YTYPE>();
        if (values.size() > 0) {
            v = sum() / values.size();
        }
        return v;
    }

    // Definitions
    template class Series<double, float>;
    template class Series<int64_t, float>;
    template class Series<double, double>;
    template class Series<int64_t, double>;
}
