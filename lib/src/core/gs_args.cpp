/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <regex>

#include "gs_args.h"

namespace Geostack
{
namespace Args
{

    /**
    * Command line argument parser
    * @param argc argument count
    * @param argv pointer to array of character strings
    * @return map of arguments and values
    */
    std::map<std::string, std::vector<std::string> > parse(int argc, char **argv) {
        
        // Create argument map
        std::map<std::string, std::vector<std::string> > args;
        args[""] = std::vector<std::string>();

        // Parse arguments
        for (int i = 1; i < argc; i++) {
            if (std::regex_match(argv[i], std::regex("^--.*"))) {
            
                // Get argument
                std::string arg = argv[i];

                // Parse argument values
                std::vector<std::string> values;
                for (int j = i+1; j < argc; i++, j++) {

                    // Get value
                    std::string value = argv[j];
                    if (std::regex_match(value, std::regex("^--.*"))) {
                        break;
                    }

                    // Add to argument list
                    values.push_back(value);
                }

                // Create map entry
                args[arg] = values;

            } else {

                // Add to default arguments
                args[""].push_back(argv[i]);
            }
        }

        // Return map
        return args;
    }

}
}